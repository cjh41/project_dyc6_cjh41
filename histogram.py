def create_histograms():
    from preprocess_image import read_image, get_color, convert_grayscale
    import matplotlib.pyplot as plt
    import numpy as np

    for i in range(12):
        name = 'histograms/healthy' + str(i).zfill(2)
        tif_filename = name + '.tif'

        im = read_image(tif_filename)

        red = get_color(im, 'red').flatten()
        red = red[np.where(red > 0)]
        red = red[np.where(red <= 240)]

        green = get_color(im, 'green').flatten()
        green = green[np.where(green > 0)]
        green = green[np.where(green <= 240)]

        blue = get_color(im, 'blue').flatten()
        blue = blue[np.where(blue > 0)]
        blue = blue[np.where(blue <= 240)]

        gray = convert_grayscale(im).flatten()
        gray = gray[np.where(gray > 0)]
        gray = gray[np.where(gray <= 240)]

        fig = plt.figure(1)
        plt.subplot(2, 2, 1)
        plt.hist(red.flatten(), bins=256)
        plt.title('Red')
        plt.xlim((0, 255))

        plt.subplot(2, 2, 2)
        plt.hist(green.flatten(), bins=256)
        plt.title('Green')
        plt.xlim((0, 255))

        plt.subplot(2, 2, 3)
        plt.hist(blue.flatten(), bins=256)
        plt.title('Blue')
        plt.xlim((0, 255))

        plt.subplot(2, 2, 4)
        plt.hist(gray.flatten(), bins=256)
        plt.title('Gray')
        plt.xlim((0, 255))

        fig.savefig(name + '.png')
        fig.clf()

    for i in range(8):
        name = 'histograms/dysplasia' + str(i).zfill(2)
        tif_filename = name + '.tif'

        im = read_image(tif_filename)

        red = get_color(im, 'red').flatten()
        red = red[np.where(red > 0)]
        red = red[np.where(red <= 240)]

        green = get_color(im, 'green').flatten()
        green = green[np.where(green > 0)]
        green = green[np.where(green <= 240)]

        blue = get_color(im, 'blue').flatten()
        blue = blue[np.where(blue > 0)]
        blue = blue[np.where(blue <= 240)]

        gray = convert_grayscale(im).flatten()
        gray = gray[np.where(gray > 0)]
        gray = gray[np.where(gray <= 240)]

        fig = plt.figure(1)
        plt.subplot(2, 2, 1)
        plt.hist(red.flatten(), bins=256)
        plt.title('Red')
        plt.xlim((0, 255))

        plt.subplot(2, 2, 2)
        plt.hist(green.flatten(), bins=256)
        plt.title('Green')
        plt.xlim((0, 255))

        plt.subplot(2, 2, 3)
        plt.hist(blue.flatten(), bins=256)
        plt.title('Blue')
        plt.xlim((0, 255))

        plt.subplot(2, 2, 4)
        plt.hist(gray.flatten(), bins=256)
        plt.title('Gray')
        plt.xlim((0, 255))

        fig.savefig(name + '.png')
        fig.clf()


if __name__ == '__main__':
    create_histograms()

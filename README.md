Automated Cervical Cancer Screening
===================================

This tool classifies images of the cervix taken with the Point-of-Care Tampon (POCkeT) colposcope as healthy or dysplasic using a support vector machine (SVM). The SVM is trained using image features (green mode and blue mode) to create a hyperplane that separates healthy and abnormal images. It can then be used to classify test images.

Instructions for Use
--------------------
* Clone this repository:
  + ``git clone git@gitlab.oit.duke.edu:cjh41/project_dyc6_cjh41.git``
* For help on how to use the program:
  + ``python main.py -h``
* All healthy images should be contained in a single folder, and the folder name can be specified in one of the program's input arguments (the default is "healthy"). In the folder, images should be numbered with a prefix that is the same as the folder name (for example, "healthy00.tif", "healthy01.tif", etc.).
* All dysplasic images should be contained in a single folder, and the folder name can be specified in one of the program's input arguments (the default is "dysplasia"). In the folder, images should be numbered with a prefix that is the same as the folder name (for example, "dysplasia00.tif", "dysplasia01.tif", etc.).
* At least 10 healthy images and 10 dysplasic images should be used to train the SVM.
* The numbers of healthy and dysplasic images used to train the SVM should be relatively balanced (difference should be no more than 50%).

Project Development
-------------------
* We began the project by implementing the suggested feature extraction and classification workflow. This involved the following steps:
  + Reading in a pre-segmented .tif image as an array.
  + Extracting the blue and green channels from the RGB image. We also included the ability to extract the red channel as well as to obtain the grayscale image, in case these would be later used for feature extraction.
  + Eliminating zero pixel values from the blue and green channels and replacing the values with 2D interpolated values from scipy's griddata method, while keeping the segment mask values at zero.
  + Eliminating specular reflection (blue channel > 240) values and replacing the values with 2D interpolated values from scipy's griddata method.
  + Calculating the mode of the blue channel, and selecting the lowest mode in the event that there was more than one mode for greater sensitivty.
  + Calculating the global pixel threshold value of the green channel using Otsu's thresholding method.
  + Using the blue mode and green threshold from the images to train half the data on the SVM and use the other half to test the algorithm.
* We found that the 2D interpolation was taking too long for images with large amounts of specular reflection.
  + We also tried 1-dimensional row-wise lienar and cubic spline interpolation to replace the specular reflection values, but found that it was not producing meaningful data and would potentially skew the information contained in the image.
  + We later dealt with this problem by excluding any images that contained more than a specified proportion of specular reflection pixels.
* Using this workflow and a total of 12 healthy images and 8 dysplasic images, we obtained only 40% accuracy with the SVM. We therefore explored different parameters to see whether this could improve the results.
  + First, we wrote code to generate the pixel histograms for each of the RGB and gray channels for each image. However, it was difficult to determine relevant parameters based on inspection of the histograms.
  + We extracted the mean, median, mode, variance, and threshold from the RGB and gray channels for each image, and ran 190 SVM analyses using each unique combination of two parameters.
  + The first half of the healthy and dysplasic data was used to train the SVM while the second half was used to test the SVM.
  + While we found certain combinations of parameters that resulted in 70-80% accuracy, this result was not necessarily maintained when we switched the training and the test data.
* We turned to a new method to validate the results of the SVM: bootstrapping. We also used new training data: 49 images that were specifically labeled as "healthy" and 48 images that were specifically labeled as "dysplasic".
  + For training, we sampled 49 healthy images with replacement, and 48 dysplasic images with replacement.
  + For testing, we again sampled 49 healthy images with replacement, and 48 dysplasic images with replacement.
  + This method allowed us to artificially increase our sample size, since we did not have many images for training.
  + We also standardized the input data using scikit-learn's scale method, which greatly improved the computational efficiency of the SVM.
  + Still using the 190 combinations of parameters described above, we ran the bootstrap algorithm 100 times and obtained the average overall accuracy, heatlhy accuracy, and dysplasic accuracy for each combination of parameters.
  + We wanted high sensitivity, so we selected green mode and blue mode, which had the highest dysplasic accuracy (about 85%) and reasonable healthy accuracy (around 50%).
* Finally, we modified our code to extract only the green mode and blue mode from each image and run the SVM with a bootstrapping algorithm.
  + We added exceptions to account for cases in which not enough data or unbalanced data was presented to the SVM.
  + We also created a main file in which the user can specify the folder names for the healthy and dysplasic images, the total number of healthy and dysplasic images, and the number of images that should be used to train the SVM.

License
-------
This software is licensed under the MIT license. See LICENSE file for more details.

Contributors
------------
* Collyn Heier (cjh41@duke.edu)
* Derek Chan (dyc6@duke.edu)
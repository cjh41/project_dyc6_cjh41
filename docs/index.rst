.. cervix documentation master file, created by
   sphinx-quickstart on Fri Dec 16 01:56:01 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cervix's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

   main
   preprocess_image
   process_image
   SVM


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`


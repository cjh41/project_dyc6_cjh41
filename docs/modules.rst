project_dyc6_cjh41
==================

.. toctree::
   :maxdepth: 4

   SVM
   histogram
   main
   parameters
   preprocess_image
   process_image
   test_SVM
   test_preprocess_image
   test_process_image

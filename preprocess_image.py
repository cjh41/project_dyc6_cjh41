def read_image(filename):
    """Reads TIF image file and imports it as a numpy array

    :param filename: name of TIF image file (str)
    :return: image: m x n x 3 image
    """
    import numpy as np
    import sys
    import errno
    import imghdr
    import logging
    from scipy import misc

    from PIL import Image
    try:
        image = misc.imread(filename)
    except FileNotFoundError:
        logging.error('The TIF image file could not be found')
        sys.exit('The TIF image file could not be found')
    except (IOError, OSError) as ex:
        if ex.errno == errno.ENOSPC:
            logging.error('Ran out of space while trying to open TIF file.')
            sys.exit('Ran out of space while trying to open TIF file.')

    # check file type
    extension = imghdr.what(filename)
    if extension != 'tiff':
        logging.error('File is not a TIF image')
        sys.exit('File is not a TIF image')

    logging.info('Read in TIF image file.')
    return image


def get_color(image, color_name='red'):
    """Extracts color channel of input image. If wrong color name is specified,
    red channel is assumed.

    :param image: image from which to extract color channel (np.array)
    :param color_name: color to extract (string: 'red', 'green', or 'blue')
    :return: channel_img: 2D array containing specified color data (np.array)
    """

    import logging

    # create numeric variable specifying color (0=red, 1=green, 2=blue)
    if color_name == 'red':
        color = 0
    elif color_name == 'green':
        color = 1
    elif color_name == 'blue':
        color = 2
    else:
        color = 0
        logging.debug('Incorrect color name, assuming red channel.')

    channel_img = image[:, :, color]

    logging.info('Extracted color channel of image.')

    return channel_img


def remove_zeros(image):
    """Takes out zero values from image to prevent them from being processed.

    :param image: 2D input image (np.array)
    :return: image_no_zeros: image with zeros removed (np.array)
    """

    import logging

    image_nonzero = image[image > 0]

    logging.info('Removed zero values from image.')

    return image_nonzero


def remove_reflection(image, reflection_thresh=0.3):
    """Takes out specular reflection values from the image. If threshold for
    proportion of reflection values is exceeded, an empty array is returned.

    :param image: 2D input image (np.array)
    :param reflection_thresh: max threshold for proportion of reflection values
     (int)
    :return: image_no_reflection: image without specular reflection (np.array)
    """

    import numpy as np
    import logging

    image_no_reflection = image[image <= 240]

    if image_no_reflection.size < (1-reflection_thresh) * image.size:
        image_no_reflection = np.array([])
        logging.debug('Removed image with too much reflection.')

    logging.info('Removed specular reflection values from image.')

    return image_no_reflection


if __name__ == '__main__':
    pass

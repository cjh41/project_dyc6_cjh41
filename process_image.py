# def threshold(image):
#
#     import skimage.filters
#     import numpy as np
#     import logging
#
#     # ignore zero values in segment mask
#     image_nonzero = image[np.nonzero(image)]
#
#     thresh = skimage.filters.threshold_otsu(image_nonzero)
#
#     logging.info('Determined the image threshold using Otsu thresholding.')
#
#     return thresh


def find_mode(image):
    """Calculates the mode of the image after removing zero and reflection
    values. If too many reflection values are present, the image is ignored
    and the unused mode is set to -1.

    :param image: 2D input image (np.array)
    :return: ignore_image: boolean indicating if image should be ignored.
    :return: mode: mode of the image (int)
    """

    from preprocess_image import remove_zeros, remove_reflection
    from scipy import stats
    import logging

    image_no_zeros = remove_zeros(image)  # ignore zero values
    image_no_spec = remove_reflection(image_no_zeros)  # ignore reflection
    m = stats.mode(image_no_spec, axis=None)  # finds modes and their counts
    mode_array = m[0]  # extract the modes

    if mode_array.size == 0:
        ignore_image = 1
        mode = -1
    else:
        ignore_image = 0
        mode = min(mode_array)  # take the smallest mode for higher sensitivity

    logging.info('Found mode of image.')
    return ignore_image, mode


# def find_stats(image):
#
#     import numpy as np
#     from scipy import stats
#     import statistics
#
#     # image = image[image != 0]  # ignore zero values in image
#     # image = image[image <= 240]  # ignore specular reflection values in
# image
#     image = image.astype(np.float)
#     median = statistics.median(image.flatten())
#     var = statistics.pvariance(image.flatten())
#     mean = statistics.mean(image.flatten())
#
#     m = stats.mode(image, axis=None)  # finds the modes as well as their
# counts
#     mode_array = m[0]  # extract the modes
#     if mode_array.size > 1:
#         mode = min(mode_array)  # take the smallest mode for higher
# sensitivity
#     else:
#         mode = mode_array
#
#     mode = mode[0]
#
#     median = round(median, 2)
#     var = round(var, 2)
#     mean = round(mean, 2)
#     return mode, median, var, mean


if __name__ == '__main__':
    pass

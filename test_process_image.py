# def test_threshold():
#     from process_image import threshold
#     import numpy as np
#
#     """
#     The tests for Otsu's thresholding method are modified from the
# calculation
#     of a known threshold for a sample test image, which can be found here:
#     http://www.labbookpages.co.uk/software/imgProc/otsuThreshold.html.
#     Note: the threshold at this website is defined to be the lower threshold
#     value for foreground, while the skimage method for finding threshold
#     returns the upper threshold value for background.
#     """
#
#     test_img = np.array([[10, 10, 11, 14, 14, 15],
#                          [10, 11, 13, 14, 13, 14],
#                          [11, 13, 14, 12, 11, 13],
#                          [14, 14, 13, 11, 10, 10],
#                          [15, 14, 12, 11, 10, 10],
#                          [15, 15, 14, 13, 11, 10]])
#
#     assert threshold(test_img) == 12
#
#
# def test_threshold_withmask():
#     from process_image import threshold
#     import numpy as np
#
#     test_img = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
#                          [0, 0, 10, 10, 11, 14, 14, 15, 0, 0],
#                          [0, 0, 10, 11, 13, 14, 13, 14, 0, 0],
#                          [0, 0, 11, 13, 14, 12, 11, 13, 0, 0],
#                          [0, 0, 14, 14, 13, 11, 10, 10, 0, 0],
#                          [0, 0, 15, 14, 12, 11, 10, 10, 0, 0],
#                          [0, 0, 15, 15, 14, 13, 11, 10, 0, 0],
#                          [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]])
#
#     assert threshold(test_img) == 12


def test_mode():
    from process_image import find_mode
    import numpy as np

    test_img = np.array([[0, 1, 2, 3, 3, 5, 6, 0, 0],
                         [0, 1, 2, 3, 3, 5, 6, 0, 0],
                         [0, 1, 2, 3, 3, 5, 6, 0, 0],
                         [0, 1, 2, 3, 3, 5, 6, 0, 0],
                         [0, 1, 2, 3, 3, 5, 6, 0, 0]])
    ignore_image, mode_test = find_mode(test_img)
    assert ignore_image == 0
    assert mode_test == 3


def test_multiple_modes():
    from process_image import find_mode
    import numpy as np

    # create array with two modes, 3 and 6. Smaller mode should be returned.
    test_img = np.array([[1, 2, 3, 3, 6, 6],
                         [1, 2, 3, 3, 6, 6],
                         [1, 2, 3, 3, 6, 6],
                         [1, 2, 3, 3, 6, 6],
                         [1, 2, 3, 3, 6, 6]])
    ignore_image, mode_test = find_mode(test_img)
    assert ignore_image == 0
    assert mode_test == 3


def test_mode_reflection():
    from process_image import find_mode
    import numpy as np

    test_img = np.array([[1, 2, 3, 3, 6, 6],
                         [1, 250, 250, 250, 250, 6],
                         [1, 245, 245, 245, 245, 6],
                         [1, 245, 245, 245, 242, 6],
                         [1, 2, 3, 3, 6, 6]])
    ignore_image, mode_test = find_mode(test_img)
    assert ignore_image == 1
    assert mode_test == -1


# def test_find_stats():
#     from process_image import find_stats
#     import numpy as np
#
#     test_img = np.array([[1, 2, 3, 4, 6, 6],
#                          [1, 2, 3, 4, 6, 6],
#                          [1, 2, 3, 4, 6, 6],
#                          [1, 2, 3, 4, 6, 6],
#                          [1, 2, 3, 4, 6, 6]])
#
#     mode, median, var, mean = find_stats(test_img)
#     assert mode == 6
#     assert median == 3.5
#     assert var == 3.56
#     assert mean == 3.67

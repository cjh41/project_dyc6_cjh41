def test_svm_check():
    from SVM import svm_check
    import numpy as np

    test_y = [1, 0, 1, 0, 0, 0, 1, 1, 1, 0]
    true_y = [1, 0, 1, 0, 1, 0, 1, 0, 1, 0]
    accuracy = svm_check(test_y, true_y)
    assert accuracy == 80

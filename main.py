import logging
import sys
from preprocess_image import read_image, get_color
from process_image import find_mode
from SVM import *

logging.basicConfig(filename='log.txt', level=logging.DEBUG,
                    format='%(asctime)s %(message)s',
                    datefmt='%m/%d/%Y %I:%M:%S %p')


def parse_cli():
    """ parse CLI

    :returns: args
    """

    import argparse as ap
    par = ap.ArgumentParser(description="Uses a support vector machine to"
                                        " classify a repository of cervix "
                                        "images as 'healthy' or 'dysplasia' "
                                        "based on the mode of the blue and "
                                        "green channels of the images.",
                            formatter_class=ap.ArgumentDefaultsHelpFormatter)

    par.add_argument("-f", "--healthy_folder", dest="healthy_folder",
                     help="name of folder containing healthy cervix images",
                     type=str, default='healthy')  # change the folder name

    par.add_argument("-m", "--healthy_num", dest="healthy_num",
                     help="number of healthy images to be sampled in training"
                          " data set.",
                     type=int, default=49)

    par.add_argument("-t", "--healthy_total", dest="healthy_total",
                     help="total of healthy images in healthy folder",
                     type=int, default=49)

    par.add_argument("-d", "--dysplasia_folder", dest="dysplasia_folder",
                     help="name of folder containing dysplasia cervix images",
                     type=str, default='dysplasia')

    par.add_argument("-n", "--dysplasia_num", dest="dysplasia_num",
                     help="number of dysplasia images to be sampled in "
                          "training data set.",
                     type=int, default=48)

    par.add_argument("-u", "--dysplasia_total", dest="dysplasia_total",
                     help="total of dysplasia images in dysplasia folder",
                     type=int, default=48)

    args = par.parse_args()
    return args


if __name__ == '__main__':

    args = parse_cli()
    healthy_folder = args.healthy_folder
    healthy_num = args.healthy_num
    healthy_total = args.healthy_total
    dysplasia_folder = args.dysplasia_folder
    dysplasia_num = args.dysplasia_num
    dysplasia_total = args.dysplasia_total

    if healthy_num / dysplasia_num > 1.5 or healthy_num / dysplasia_num < 0.5:
        logging.error('Data is unbalanced.')
        sys.exit()

    healthy_train = get_params(healthy_folder, healthy_num, healthy_total)
    dysplasia_train = get_params(dysplasia_folder, dysplasia_num,
                                 dysplasia_total)
    x_train = healthy_train + dysplasia_train

    healthy_test = get_params(healthy_folder, healthy_num, healthy_total)
    dysplasia_test = get_params(dysplasia_folder, dysplasia_num,
                                dysplasia_total)
    x_test = healthy_test + dysplasia_test

    y_train = [0]*(healthy_num + dysplasia_num)
    y_train[healthy_num:healthy_num+dysplasia_num] = [1]*dysplasia_num

    results = svm_classify(x_train, y_train, x_test)
    overall_accuracy = svm_check(results, y_train)
    healthy_accuracy = svm_check(
        results[0:healthy_num], y_train[0:healthy_num])
    dysplasia_accuracy = svm_check(
        results[healthy_num:dysplasia_num + healthy_num],
        y_train[healthy_num:dysplasia_num + healthy_num])

    print('Overall classification accuracy: %.2f' % overall_accuracy)
    print('Healthy classification accuracy: %.2f' % healthy_accuracy)
    print('Dysplasia classification accuracy: %.2f' % dysplasia_accuracy)

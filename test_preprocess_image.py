def test_read_image_dimensions():
    from preprocess_image import read_image
    import numpy as np

    filename = 'a_image.tif'
    test_image = read_image(filename)
    known_dimensions = np.array([44, 330, 3])
    test_dimensions = np.shape(test_image)
    assert np.array_equal(known_dimensions, test_dimensions)


def test_read_image_filename():
    from preprocess_image import read_image
    import pytest
    import tempfile

    tmpdir = tempfile.TemporaryDirectory()
    temp_filepath = tmpdir.name + '/temp.tif'

    with pytest.raises(SystemExit) as cm:
        image_data = read_image(temp_filepath)
    assert 'The TIF image file could not be found' in str(cm.value)


def test_read_image_type():
    from preprocess_image import read_image
    import pytest
    import tempfile

    with pytest.raises(SystemExit) as cm:
        image_data = read_image('Test.png')
    assert 'File is not a TIF image' in str(cm.value)


def test_get_color_blue():
    from preprocess_image import get_color
    import numpy as np

    test_img = np.zeros((10, 10, 3), dtype=np.uint8)
    actual_img = np.zeros((10, 10), dtype=np.uint8)
    for i in range(10):
        for j in range(10):
            test_img[i][j] = [0, i * j, i + j]
            actual_img[i][j] = i + j

    test_img_blue = get_color(test_img, 'blue')

    assert np.array_equal(test_img_blue, actual_img)


def test_get_color_green():
    from preprocess_image import get_color
    import numpy as np

    test_img = np.zeros((10, 10, 3), dtype=np.uint8)
    actual_img = np.zeros((10, 10), dtype=np.uint8)
    for i in range(10):
        for j in range(10):
            test_img[i][j] = [0, i * j, i + j]
            actual_img[i][j] = i * j

    test_img_green = get_color(test_img, 'green')

    assert np.array_equal(test_img_green, actual_img)


def test_get_color_notacolor():
    from preprocess_image import get_color
    import numpy as np

    test_img = np.zeros((10, 10, 3), dtype=np.uint8)
    actual_img = np.zeros((10, 10), dtype=np.uint8)
    for i in range(10):
        for j in range(10):
            test_img[i][j] = [i * j, i + j, 0]
            actual_img[i][j] = i * j

    test_img_notacolor = get_color(test_img, 'thisisnotacolor')

    assert np.array_equal(test_img_notacolor, actual_img)


def test_remove_zeros():
    from preprocess_image import remove_zeros
    import numpy as np

    test_img = np.array([[1, 2, 3, 4, 5, 6],
                         [1, 2, 0, 0, 5, 6],
                         [1, 2, 3, 4, 5, 6]])

    test_values = remove_zeros(test_img)

    assert np.count_nonzero(test_values) == 16


def test_remove_zeros_values():
    from preprocess_image import remove_zeros
    import numpy as np

    test_img = np.array([[1, 2, 3, 4, 5, 6],
                         [1, 2, 0, 0, 5, 6],
                         [1, 2, 3, 4, 5, 6]])
    actual_values = np.array([1, 2, 3, 4, 5, 6, 1, 2, 5, 6, 1, 2, 3, 4, 5, 6])

    test_values = remove_zeros(test_img)

    assert np.array_equal(test_values, actual_values)


def test_remove_zeros_mask():
    from preprocess_image import remove_zeros
    import numpy as np

    test_img = np.array([[0, 0, 0, 0, 0, 0, 0, 0],
                         [0, 1, 2, 3, 4, 5, 6, 0],
                         [0, 1, 2, 0, 0, 5, 6, 0],
                         [0, 1, 2, 3, 4, 5, 6, 0],
                         [0, 0, 0, 0, 0, 0, 0, 0]])
    actual_values = np.array([1, 2, 3, 4, 5, 6, 1, 2, 5, 6, 1, 2, 3, 4, 5, 6])

    test_values = remove_zeros(test_img)

    assert np.array_equal(test_values, actual_values)


def test_remove_reflection():
    from preprocess_image import remove_reflection
    import numpy as np

    test_img = np.array([[1, 2, 3, 4, 5, 6],
                         [1, 2, 280, 256, 5, 6],
                         [1, 2, 3, 4, 5, 6]])

    test_values = remove_reflection(test_img)
    check = np.extract([test_values <= 240], test_values)
    assert check.size == 16


def test_remove_reflection_values():
    from preprocess_image import remove_reflection
    import numpy as np

    test_img = np.array([[1, 2, 3, 4, 5, 6],
                         [1, 2, 656, 774, 5, 6],
                         [1, 2, 3, 4, 5, 6]])
    actual_values = np.array([1, 2, 3, 4, 5, 6, 1, 2, 5, 6, 1, 2, 3, 4, 5, 6])

    test_values = remove_reflection(test_img)
    assert np.array_equal(test_values, actual_values)

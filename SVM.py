def get_params(folder_name='healthy', sample_num=49, sample_total=49):
    """Reads in the images in the specified folders, samples them using a
    bootstrap method, and extracts the modes of the green and blue color
    channels to be used in SVM classification.

    :param folder_name: filename of the folder containing images (str)
    :param sample_num: number of images to be used in sampling (int)
    :param sample_total: total number of images that can be sampled (int)
    :return: mode_matrix: matrix of green and blue channel modes (list)
    """
    from preprocess_image import read_image, get_color
    from process_image import find_mode
    import random
    import logging
    import numpy as np
    import sys

    # require at least 10 images for training SVM
    if sample_num < 10:
        logging.error('Need at least 10 images in each category to train SVM.')
        sys.exit()

    mode_matrix = [[None] * 2 for _ in range(sample_num)]

    if folder_name == 'healthy' or 'dysplasia':
        for i in range(sample_num):
            num = random.randrange(0, sample_total, 1)
            print(num)
            name = folder_name + '/' + folder_name + str(num).zfill(2) + '.tif'
            image = read_image(name)
            green_channel = get_color(image, 'green')
            blue_channel = get_color(image, 'blue')
            ignore_green, green_mode = find_mode(green_channel)
            ignore_blue, blue_mode = find_mode(blue_channel)
            while ignore_green == 1 or ignore_blue == 1:
                num = random.randrange(0, sample_num, 1)
                name = folder_name + '/' + folder_name + str(num).zfill(2) + \
                    '.tif'
                image = read_image(name)
                green_channel = get_color(image, 'green')
                blue_channel = get_color(image, 'blue')
                ignore_green, green_mode = find_mode(green_channel)
                ignore_blue, blue_mode = find_mode(blue_channel)
            mode_matrix[i][0] = green_mode
            mode_matrix[i][1] = blue_mode
    else:
        logging.error('Input image folder was not "healthy" or'
                      ' "dysplasia."')
        sys.exit()
    logging.info('Sampled images and found green and blue modes.')
    return mode_matrix


def svm_classify(train_x, train_y, test_x):
    """Uses a support vector machine to classify cervix images based on the
    parameters in the input x and y vectors.

    :param train_x: list of input values from the training data (list)
    :param train_y: list of training data results (list)
    :param test_x: list of input values from the test data (list)
    :return: test_y: list of results from the test data (list)
    """

    from sklearn import svm, preprocessing
    import logging

    x_train_scaled = preprocessing.scale(train_x)
    x_test_scaled = preprocessing.scale(test_x)

    clf = svm.SVC(kernel='linear')
    clf.fit(x_train_scaled, train_y)
    test_y = clf.predict(x_test_scaled)

    logging.info('Classified test data using SVM.')
    return test_y


def svm_check(test_y, true_y):
    """Checks the accuracy of the SVM classification performed on the test data
    using known information.

    :param test_y: list of SVM results of the test data (list)
    :param true_y: list of known classification results (list)
    :return: accuracy: percent of test data predicted correctly (int)
    """
    import numpy as np
    import logging

    # convert list to numpy array for element-wise, ordered comparison
    test_array = np.array(test_y)
    true_array = np.array(true_y)
    diff = test_array - true_array

    # find the location of the nonzero (mismatched) values
    error_loc = np.nonzero(diff)
    error_num = error_loc[0].size
    known_size = true_array.size

    # compute percent error
    accuracy = (known_size - error_num)/known_size * 100

    logging.info('Checked accuracy of SVM classification.')
    return accuracy

if __name__ == '__main__':
    pass
